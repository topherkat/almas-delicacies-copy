package com.zuitt.almasdelicacies;

public class Constants {
    public static final String API_SECRET_KEY = "almas-delicacies-api";
    public static final long TOKEN_VALIDITY = 5 * 60 * 60 * 1000;
}
